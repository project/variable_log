<?php

/**
 * @file
 * Primarily Drupal hooks.
 *
 * This is the main module file for Variable log.
 */

/**
 * Implements hook_views_api().
 */
function variable_log_menu() {
  return array(
    'admin/config/system/variable-log' => array(
      'title' => 'Variable log',
      'description' => 'Settings for variable logging.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('variable_log_settings_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'variable_log.admin.inc',
    ),
    'admin/reports/variable-log/%' => array(
      'title' => 'Details',
      'page callback' => 'variable_log_details_page_callback',
      'page arguments' => array(3),
      'access arguments' => array('administer site configuration'),
      'file' => 'variable_log.admin.inc',
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function variable_log_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'variable_log') . '/views',
  );
}

/**
 * Implements hook_cron().
 */
function variable_log_cron() {
  // Cleanup the variable_log table.
  $cleanup_type = variable_get('variable_log_cleanup_type', 'row_limit');

  switch ($cleanup_type) {
    case 'row_limit':
      if ($row_limit = variable_get('variable_log_row_limit', 1000)) {
        // For row limit n, get the id of the nth row in descending id order.
        // Counting the most recent n rows avoids issues with id number
        // sequences, e.g. auto_increment value > 1 or rows deleted directly
        // from the table.
        $min_row = db_select('variable_log', 'v')
          ->fields('v', array('id'))
          ->orderBy('id', 'DESC')
          ->range($row_limit - 1, 1)
          ->execute()
          ->fetchField();

        // Delete all table entries older than the nth row,
        // if nth row was found.
        if ($min_row) {
          db_delete('variable_log')
            ->condition('id', $min_row, '<')
            ->execute();
        }
      }
      break;

    case 'timestamp_limit':
      if ($timestamp_limit = variable_get('variable_log_timestamp_limit', 2592000)) {
        db_delete('variable_log')
          ->condition('timestamp', REQUEST_TIME - $timestamp_limit, '<')
          ->execute();
      }
      break;
  }
}

/**
 * Implements hook_variable_update().
 */
function variable_log_variable_update($name, $value, $old_value, $options) {
  if (is_array($value) && is_array($old_value)) {
    $updated = array_diff($value, $old_value);
  }
  else {
    $updated = $value != $old_value;
  }

  if ($updated) {
    $context = array(
      'name' => $name,
      'value' => $value,
      'old_value' => $old_value,
      'options' => $options,
    );
    drupal_alter('variable_log_variable_update', $updated, $context);

    if ($updated) {
      variable_log_record_save($name, $value, $old_value, $options);
    }
  }
}

/**
 * Form constructor for the clear form.
 *
 * @see variable_log_clear_form()
 * @ingroup forms
 */
function variable_log_clear_form($form, $form_state) {
  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log'),
  );

  return $form;
}

/**
 * Form submission handler for variable_log_clear_form().
 *
 * @see variable_log_clear_form()
 */
function variable_log_clear_form_submit($form, &$form_state) {
  db_truncate('variable_log')
    ->execute();

  drupal_set_message(t('Variable log cleared.'));
}

/**
 * Logs a variable update.
 *
 * @param string $name
 *   The variable name.
 * @param mixed $value
 *   The value of variable.
 * @param mixed $old_value
 *   The old value of variable.
 * @param array $options
 *   The options of variable.
 */
function variable_log_record_save($name, $value, $old_value, $options) {
  global $user;

  db_insert('variable_log')
    ->fields(array(
      'uid' => $user->uid,
      'name' => $name,
      'old_value' => serialize($old_value),
      'value' => serialize($value),
      'options' => serialize($options),
      'timestamp' => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * Load a database log by id.
 *
 * @param int $id
 *   Unique ID of the database log.
 *
 * @return array
 *   An array contains loaded database log.
 */
function variable_log_record_load($id) {
  $result = db_select('variable_log', 'v')
    ->fields('v', array('name', 'old_value', 'value'))
    ->condition('v.id', $id)
    ->execute()
    ->fetchAssoc();

  return $result;
}
