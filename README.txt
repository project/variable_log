-- SUMMARY --
Allows you see who and when updated a value of variable.

-- CONFIGURATION --
After enabling a module, go to admin/config/system/variable-log and set up
a method to clear the log

-- REQUIREMENTS --
http://www.drupal.org/project/diff
http://www.drupal.org/project/variable
http://www.drupal.org/project/views

-- CONTACT --
Current maintainer:
* Harbuzau Yauheni - http://drupal.org/user/2123020

-- SPONSORS --
Adyax - http://www.adyax.com/
